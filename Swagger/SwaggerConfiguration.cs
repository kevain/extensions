﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace kevain.Extensions.Swagger
{
    /// <summary>
    ///     Extensions for SwaggerGenOptions
    /// </summary>
    public static class SwaggerConfiguration
    {
        /// <summary>
        ///     Get XML documentation from assembly and include in swagger generation
        /// </summary>
        /// <param name="options">SwaggerGenOptions</param>
        public static void EnableXMLDocumentationComments(this SwaggerGenOptions options)
        {
            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

            options.IncludeXmlComments(xmlPath);
        }

        /// <summary>
        ///     Add API version descriptions to Swagger documentation
        /// </summary>
        /// <param name="options">SwaggerGenOptions</param>
        /// <param name="apiVersionDescriptions">API version descriptions</param>
        /// <param name="title">Title</param>
        /// <param name="description">Description</param>
        public static void AddApiVersionDescriptions(this SwaggerGenOptions options,
            IEnumerable<ApiVersionDescription> apiVersionDescriptions, string title, string? description)
        {
            foreach (var apiVersionDescription in apiVersionDescriptions)
                options.AddApiVersionDescription(apiVersionDescription, title, description);
        }

        // ReSharper disable once MemberCanBePrivate.Global
        /// <summary>
        ///     Add API version description to Swagger documentation
        /// </summary>
        /// <param name="options">SwaggerGenOptions</param>
        /// <param name="apiVersionDescription">API version description</param>
        /// <param name="title">Title</param>
        /// <param name="description">Description</param>
        public static void AddApiVersionDescription(this SwaggerGenOptions options,
            ApiVersionDescription apiVersionDescription,
            string title, string? description)
        {
            options.SwaggerDoc(apiVersionDescription.GroupName, new OpenApiInfo
            {
                Title = title,
                Version = apiVersionDescription.ApiVersion.ToString(),
                Description = description
            });
        }

        /// <summary>
        ///     Add security definition and requirement for bearer to Swagger
        /// </summary>
        /// <param name="options">SwaggerGenOptions</param>
        public static void AddBearerSecurityDefinitionAndRequirement(this SwaggerGenOptions options)
        {
            options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                Name = "Authorization",
                Description =
                    "JWT Authorization header using the bearer scheme. " +
                    "Enter 'bearer' followed by your token.",
                In = ParameterLocation.Header,
                Type = SecuritySchemeType.ApiKey,
                Scheme = "Bearer"
            });

            options.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        },
                        Scheme = "OAuth2",
                        Name = "Bearer",
                        In = ParameterLocation.Header
                    },
                    new List<string>()
                }
            });
        }
    }
}