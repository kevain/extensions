# Extensions

Collection of commonly used extensions to share between different solutions.

## NuGet links

[Identity extensions](https://www.nuget.org/packages/kevain.Extensions.Identity/). 
[Swagger extensions](https://www.nuget.org/packages/kevain.Extensions.swagger/). 
