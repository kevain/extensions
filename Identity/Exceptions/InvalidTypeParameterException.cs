﻿using System;

namespace kevain.Extensions.Identity.Exceptions
{
    /// <summary>
    ///     Custom exception for handling invalid type parameters
    /// </summary>
    public class InvalidTypeParameterException : Exception
    {
        /// <summary>
        ///     Initialize new exception without error message
        /// </summary>
        public InvalidTypeParameterException()
        {
        }

        /// <summary>
        ///     Initialize new exception with an error message
        /// </summary>
        /// <param name="message">Error message to describe why exception was thrown</param>
        public InvalidTypeParameterException(string message) : base(message)
        {
        }
    }
}