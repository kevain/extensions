﻿using System;
using System.Linq;
using System.Security.Claims;
using kevain.Extensions.Identity.Exceptions;

namespace kevain.Extensions.Identity
{
    /// <summary>
    ///     Contains extension methods for operations with the user identifier
    /// </summary>
    public static class UserIdentifier
    {
        /// <summary>
        ///     Get user's identifier
        /// </summary>
        /// <param name="appUser">user's claims principal</param>
        /// <typeparam name="TKey">identifier's type</typeparam>
        /// <returns>User's identifier</returns>
        /// <exception cref="InvalidTypeParameterException">Invalid identifier type was provided</exception>
        // ReSharper disable once MemberCanBePrivate.Global
        public static TKey GetUserId<TKey>(this ClaimsPrincipal appUser)
            where TKey : IEquatable<TKey>
        {
            var stringId = appUser.Claims
                .Single(c => c.Type == ClaimTypes.NameIdentifier).Value;

            if (typeof(TKey) == typeof(string)) return (TKey) Convert.ChangeType(stringId, typeof(TKey));

            if (typeof(TKey) == typeof(int) || typeof(TKey) == typeof(long))
                return stringId != null
                    ? (TKey) Convert.ChangeType(stringId, typeof(TKey))
                    : (TKey) Convert.ChangeType(0, typeof(TKey));

            if (typeof(TKey) == typeof(Guid)) return (TKey) Convert.ChangeType(new Guid(stringId), typeof(TKey));

            throw new InvalidTypeParameterException("Invalid user identifier type parameter provided");
        }

        /// <summary>
        ///     Get user's identifier of GUID type
        /// </summary>
        /// <param name="appUser">user's claims principal</param>
        /// <returns>user's GUID</returns>
        public static Guid GetUserId(this ClaimsPrincipal appUser) => appUser.GetUserId<Guid>();
    }
}