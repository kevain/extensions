﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace kevain.Extensions.Identity
{
    /// <summary>
    ///     JWT extensions
    /// </summary>
    public static class JSONWebToken
    {
        /// <summary>
        ///     Generate JSON Web Token
        /// </summary>
        /// <param name="claimsPrincipal">User's claims principal</param>
        /// <param name="signingKey">Token signing key</param>
        /// <param name="issuer">Token issuer</param>
        /// <param name="expirationEnd">Token expiration datetime</param>
        /// <param name="securityAlgorithm">Security algorithm for token signing</param>
        /// <returns>Generated token as string</returns>
        public static string GenerateJWT(this ClaimsPrincipal claimsPrincipal, string signingKey, string issuer,
            DateTime expirationEnd, string securityAlgorithm = SecurityAlgorithms.HmacSha512)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(signingKey));
            var signingCredentials = new SigningCredentials(key, securityAlgorithm);

            var token = new JwtSecurityToken(issuer, issuer, claimsPrincipal.Claims, null, expirationEnd,
                signingCredentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}